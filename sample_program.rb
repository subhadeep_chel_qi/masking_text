require 'class_method_text_masking' #implement using class method
require 'text_masking_method'       #implement using method call

TextMasking.masking_text("chelsubhade@gmail.com") # =>"c***********p@g***l.c*m"
TextMasking.masking_text("chelsubhadp@gmail.com","#") # =>"c###########p@g###l.c#m"
masking_text("chelsubhadeep@gmail.com") # => "c***********p@g***l.c*m"
masking_text("+91-8902267368") # => "+91-*********8"
masking_text("+91-8902267368","#") # => "+91-#########8"

masking_text("8902267368") # => RuntimeError (Error No :001 Invalid Email Or Mobile Number Format)
TextMasking.masking_text("+918902267368","#") # => RuntimeError (Error No :001 Invalid Email Or Mobile Number Format)
TextMasking.masking_text("+91-8902267368","$") # => RuntimeError (Error No :002 Invalid Symbol)