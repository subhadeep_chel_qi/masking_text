module User
  def self.email(text = 'chelsubhadeep@gmail.com')
    !!text.match(/\A[\w.+-]+@\w+\.\w+\z/) ? text : nil
  end
  def self.phone_num(text = '+91-8902267368')
    !!text.match(/^([+]\d{2})?-\d{10}$/) || !!text.match(/^\d{10}$/) || !!text.match(/^\d{8}$/) ? text : nil
  end
end

class TextMasking
   include  User
   def self.attr_masking(text)
     define_method(text) do
      instance_veriable_get("@#{text}")
     end

     if text == :email
        if User::email == nil
          raise 'Error No :001 Invalid Email Or Mobile Number Format'
        else
          masked_email(User::email, '#')
        end
      elsif text == :phone_number
        if User::phone_num == nil
          raise 'Error No :001 Invalid Email Or Mobile Number Format'
        else
          masked_number(User::phone_num, '#')
        end
      else
        raise 'Error No :003 Invalid Attribute'
      end
   end

   def self.masked_number(number, symbol)
    if number.include?('-')
      temp_phne_no              = number.split("-")
      region_code               = temp_phne_no[0] + "-"
      main_phone_num            = number.split("-")[1]
    else
      region_code               = ''
      main_phone_num            = number
    end
    main_phone_num_length       =  main_phone_num.length
    sym = ""
    (main_phone_num_length - 2).times { sym = sym + symbol }
    masked_main_phone_number  = main_phone_num[0] + sym + main_phone_num[main_phone_num_length - 1]
    region_code + masked_main_phone_number
  end

  def self.add_symbol(text,symbol)
    sym = ""
    (text.length - 2).times { sym += symbol }
    text[0] + sym + text[text.length - 1]
  end

  def self.masked_email(email, symbol)
    email_seperated_format        = email.split("@")
    user_name                     = email_seperated_format[0]
    host_domain_name              = email_seperated_format[1].split(".")
    host_name                     = host_domain_name[0]
    domain_name                   = host_domain_name[1]
    add_symbol(user_name, symbol) + "@" + add_symbol(host_name, symbol) + "." + add_symbol(domain_name, symbol)
  end
end

class Masking < TextMasking
  attr_masking :phone_number
  attr_masking :email
end