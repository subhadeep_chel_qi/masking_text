lib = File.expand_path(__dir__)

require_relative "#{lib}/contact_information_masking.rb"
require_relative "#{lib}/masking_contact_information.rb"