require './class_method_text_masking.rb'

RSpec.describe TextMasking do
  describe '.masking_text' do
    context 'return masked email' do
      it "returns masked email" do
        expect(TextMasking.masking_text('chelsubhadeep@gmail.com','*')).to eq(TextMasking.masked_email('chelsubhadeep@gmail.com','*'))
      end
    end
    context 'return masked phone number' do
      it "returns masked phonenumber" do
        expect(TextMasking.masking_text('+91-8902267368','*')).to eq(TextMasking.masked_number('+91-8902267368','*'))
      end
    end
    context  'given text is valid email_id' do
      let(:text) {'chelsubhadeep@gmail.com'}
      it "return true" do
        expect(text.match(/\A[\w.+-]+@\w+\.\w+\z/)).not_to eq(nil)
      end
    end
    context  'given text is valid phonenumber' do
      let(:text) {'+91-8902267368'}
      it "return true" do
        binding.pry
        expect(text.match(/^([+]\d{2})?-\d{10}$/)).not_to eq(nil)
      end
    end

  end
end