# def masked_attr(attributes)
#   if attributes == :email
#     masked_email('chelsubhadeep@gmail.com','*')
#   elsif attributes == :phone_no
#     masked_number("+91-9835456652","#")
#   end
# end



# self.send(:email) #instance method
# attr_mask :email
# def masked_number(number, symbol)
#   phone_num                 = number
#   temp_phne_no              = phone_num.split("-")
#   region_code               = temp_phne_no[0] + "-"
#   main_phone_num            = phone_num.split("-")[1]
#   main_phone_num_length     = main_phone_num.length
#   sym = ""
#   (main_phone_num_length - 3).times { sym = sym + symbol }
#   masked_main_phone_number  = sym + main_phone_num[(main_phone_num_length - 3) ... main_phone_num_length]
#   region_code + masked_main_phone_number
# end

# def add_symbol(text,symbol)
#   sym = ""
#   (text.length - 2).times { sym += symbol }
#   text[0] + sym + text[text.length - 1]
# end

# def masked_email(email, symbol)
#   email_seperated_format        = email.split("@")
#   user_name                     = email_seperated_format[0]
#   host_domain_name              = email_seperated_format[1].split(".")
#   host_name                     = host_domain_name[0]
#   domain_name                   = host_domain_name[1]
#   add_symbol(user_name, symbol) + "@" + add_symbol(host_name, symbol) + "." + add_symbol(domain_name, symbol)
# end

class TextMasking
  def self.masking_text(text, symbol = '*')
    if symbol == '*' || symbol == '#'
      if !!text.match(/\A[\w.+-]+@\w+\.\w+\z/) #/\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
        masked_email(text, symbol)
      elsif !!text.match(/^([+]\d{2})?-\d{10}$/)
        masked_number(text, symbol)
      elsif !!text.match(/^\d{10}$/)
        masked_number(text, symbol)
      elsif !!text.match(/^\d{8}$/)
        masked_number(text, symbol)
      else
        raise 'Error No :001 Invalid Email Or Mobile Number Format'
      end
    else
      raise 'Error No :002 Invalid Symbol'
    end
  end

  def self.masked_number(number, symbol)
    if number.include?('-')
      temp_phne_no              = number.split("-")
      region_code               = temp_phne_no[0] + "-"
      main_phone_num            = number.split("-")[1]
    else
      region_code               = ''
      main_phone_num            = number
    end

    main_phone_num_length       =  main_phone_num.length
    sym = ""
    (main_phone_num_length - 2).times { sym = sym + symbol }
    masked_main_phone_number  = main_phone_num[0] + sym + main_phone_num[main_phone_num_length - 1]
    region_code + masked_main_phone_number
  end

  def self.add_symbol(text,symbol)
    sym = ""
    (text.length - 2).times { sym += symbol }
    text[0] + sym + text[text.length - 1]
  end

  def self.masked_email(email, symbol)
    email_seperated_format        = email.split("@")
    user_name                     = email_seperated_format[0]
    host_domain_name              = email_seperated_format[1].split(".")
    host_name                     = host_domain_name[0]
    domain_name                   = host_domain_name[1]
    add_symbol(user_name, symbol) + "@" + add_symbol(host_name, symbol) + "." + add_symbol(domain_name, symbol)
  end
end

class Masking < TextMasking
  def self.text(text, symbol = '*')
    TextMasking.masking_text(text, symbol)
  end
end


class TextMasking
   def self.attr_masking(text)
     define_method(text) do
      instance_veriable_get("@#{text}")
     end
     masked_email('chelsubhadeep@gmail.com', '#')
   end

  def self.add_symbol(text,symbol)
    sym = ""
    (text.length - 2).times { sym += symbol }
    text[0] + sym + text[text.length - 1]
  end

  def self.masked_email(email, symbol)
    email_seperated_format        = email.split("@")
    user_name                     = email_seperated_format[0]
    host_domain_name              = email_seperated_format[1].split(".")
    host_name                     = host_domain_name[0]
    domain_name                   = host_domain_name[1]
    add_symbol(user_name, symbol) + "@" + add_symbol(host_name, symbol) + "." + add_symbol(domain_name, symbol)
  end
 end

class Masking < TextMasking
   attr_masking :email
end