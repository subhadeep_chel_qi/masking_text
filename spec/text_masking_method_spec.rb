require './text_masking_method.rb'

describe 'masking_text' do
  context 'validations for email format' do
    let(:text) {'chelsubhadeep@gmail.com'}
    it "return masked email" do
      expect(TextMasking.masking_text(text,'#')).to eq('c###########p@g###l.c#m')
    end
  end

  context 'validations for phonenumber format' do
    let(:phonenumber_with_country_code) {'+91-8902267368'}
    let(:phonenumber) {'8902267368'}
    let(:landline) {'23295655'}

    it "return masked phonenumber with country code" do
      expect(masking_text(phonenumber_with_country_code)).to eq('+91-8********8')
    end
    it "return masked phonenumber without country code" do
      expect(masking_text(phonenumber)).to eq('8********8')
    end
    it "return masked landline phone number" do
      expect(masking_text(landline)).to eq('2******5')
    end
  end
end