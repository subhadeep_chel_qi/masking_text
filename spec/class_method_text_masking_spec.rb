require './class_method_text_masking.rb'

RSpec.describe TextMasking do
  describe '.masking_text' do
    context 'validations for email format' do
      let(:text) {'chelsubhadeep@gmail.com'}
      it "return masked email" do
        expect(TextMasking.masking_text(text,'#')).to eq('c###########p@g###l.c#m')
      end
    end

    context 'validations for phonenumber format' do
      let(:phonenumber_with_country_code) {'+91-8902267368'}
      let(:phonenumber) {'8902267368'}
      let(:landline) {'23295655'}

      it "return masked phonenumber with country code" do
        expect(TextMasking.masking_text(phonenumber_with_country_code)).to eq('+91-8********8')
      end
      it "return masked phonenumber without country code" do
        expect(TextMasking.masking_text(phonenumber)).to eq('8********8')
      end
      it "return masked landline phone number" do
        expect(TextMasking.masking_text(landline)).to eq('2******5')
      end
    end
  end

  describe '.masked_number' do
    let(:phonenumber_with_country_code) { '+91-8902267368' }
    let(:phonenumber) { '8902267368' }
    let(:landline) { '23295655' }
    let(:symbol)      { '*' }

    it "return masked phonenumber with country code" do
      expect(TextMasking.masked_number(phonenumber_with_country_code, symbol)).to eq('+91-8********8')
    end
    it "return masked phonenumber without country code" do
      expect(TextMasking.masked_number(phonenumber, symbol)).to eq('8********8')
    end
    it "return masked landline phone number" do
      expect(TextMasking.masked_number(landline, symbol)).to eq('2******5')
    end
  end

  describe '.masked_email' do
    let(:text) {'chelsubhadeep@gmail.com'}
    it "returns masked email" do
      expect(TextMasking.masked_email(text,'#')).to eq('c###########p@g###l.c#m')
    end
  end

  describe '.add_symbol' do
    let(:user_name)   { 'chelsubhadeep' }
    let(:host_name)   { 'gmail' }
    let(:domain_name) { 'com' }
    let(:symbol)      { '*' }

    it "returns with adding symbols with user name" do
      expect(TextMasking.add_symbol(user_name, symbol)).to eq('c***********p')
    end

    it "returns with adding symbols with host name" do
      expect(TextMasking.add_symbol(host_name, symbol)).to eq('g***l')
    end

    it "returns with adding symbols with domain name" do
      expect(TextMasking.add_symbol(domain_name, symbol)).to eq('c*m')
    end
  end
end


#/\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
#/\A[a-zA-Z0-9.!\#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*\z/