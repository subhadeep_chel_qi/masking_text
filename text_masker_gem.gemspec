Gem::Specification.new do |s|
  s.name          = "text_masker_gem"
  s.version       = "0.0.1"
  s.licenses      = ["QI"]
  s.date          = "2020-04-01"
  s.summary       = "text_masking_gem is used for masking the text according the format"
  s.description   = "Much longer explanation of this!"
  s.authors       = ["Subhadeep Chel"]
  s.email         = "subhadeep.chel@quantuminventions.com"
  s.files         = ["lib/class_method_text_masking.rb", "lib/text_masking_method.rb"]
  s.require_paths = ["lib"]
end
