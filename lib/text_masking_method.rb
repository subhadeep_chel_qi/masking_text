require './class_method_text_masking.rb'

def masking_text(text, symbol = '*')
  TextMasking.masking_text(text, symbol)
end