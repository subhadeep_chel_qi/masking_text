require './attr_masker.rb'

class MaskedValue < Mask
  def initialize (email, phone_number)
    Info.new(email, phone_number)
  end

  mask_attr :email
  mask_attr :phone_number
end

MaskedValue.new('subhadeep.chel@gmail.com', '8902267368')
MaskedValue.masked_email
MaskedValue.masked_phone_number