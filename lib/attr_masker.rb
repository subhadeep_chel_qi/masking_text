require './class_method_text_masking.rb'

class Info                                         #using as Database
  attr_accessor :email, :phone_num, :id
  @@count = 0

  def initialize(email , phone_num, id = object_count)
    @email      = email
    @phone_num  = phone_num
    @id         = id
    @@email     = @email
    @@phone_num  = phone_num
  end

  def self.email
    @@email
  end

  def self.phone_number
    @@phone_num
  end

  def object_count
    @@count += 1
  end
end

class  Mask
  def self.mask_attr(*f)
    f.each do |f|
      define_method "masked_#{f.to_s}" do
        self.send("masked_#{f.to_s}")
      end
    end
  end

  def self.masked_email
    TextMasking.masking_text(Info.email)
  end

  def self.masked_phone_number
    TextMasking.masking_text(Info.phone_number)
  end
end